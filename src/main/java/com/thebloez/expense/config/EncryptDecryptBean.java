package com.thebloez.expense.config;

import com.thebloez.expense.encrypt.Encryptors;
import com.thebloez.expense.encrypt.TextEncryptor;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.encrypt.BouncyCastleAesCbcBytesEncryptor;
import org.springframework.security.crypto.encrypt.BytesEncryptor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.ws.WebServiceException;

@Component
public class EncryptDecryptBean {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String SALT = "thebloez@expense";
	private static final String SALT_HEX = Hex.encodeHexString(SALT.toString().getBytes());
	private static final String PASSW_KEY = "thebloez@expense";
	private static TextEncryptor encryptor;
	

	@PostConstruct
	private void init(){
		encryptor = Encryptors.text(PASSW_KEY, SALT_HEX);
//		testEncrypDecryp();
	}
	
	public String encrypt(String source){
		if(StringUtils.isBlank(source)){
			return null;
		}
		try {
			String encryptedText = encryptor.encrypt(source);
			return encryptedText;
		} catch (Exception e) {
			logger.error("Fail to encrypt " + source, e);
			throw new WebServiceException("encryption.fail");
		}
	}
	
	public String decrypt(String source){
		if(StringUtils.isBlank(source)){
			return null;
		}
		try {
			String decryptedText = encryptor.decrypt(source);
			return decryptedText;
		} catch (Exception e) {
			throw new WebServiceException("decryption.fail",e);
		}
	}

//	private void testEncrypDecryp() {
//
//		String test = "Program luar angkasa";
//		logger.debug("plain text = " + test);
//		String encryptText = encrypt(test);
//		logger.debug("encrypted  = " + encryptText );
//		logger.debug("decrypted  = " + decrypt(encryptText));
//
//
//		// TEsT decrypt using new instance of TextEncryptor
//		logger.debug(" encryptor object ID = " + System.identityHashCode(encryptor));
//		encryptor = Encryptors.text(PASSW_KEY, SALT_HEX);
//		encryptor = Encryptors.text(PASSW_KEY, SALT_HEX);
//		encryptor = Encryptors.text(PASSW_KEY, SALT_HEX);
//		logger.debug(" encryptor object ID = " + System.identityHashCode(encryptor));
//
//		test = "Program luar angkasa";
//		logger.debug("plain text = " + test);
//		//encryptText = encrypt(test);
//		logger.debug("encrypted  = " + encryptText );
//		logger.debug("decrypted  = " + decrypt(encryptText));
//
//
//
//
//		logger.debug("Using BytesEncryptor AES " );
//
//		BytesEncryptor encryptor = new BouncyCastleAesCbcBytesEncryptor(PASSW_KEY, SALT_HEX);
//
//		//encryptText = Hex.encodeHexString(encryptor.encrypt(test.getBytes()));
//				//Base64.encodeBase64String(encryptor.encrypt(test.getBytes()));
//
//		String decrypText = "";
//		try {
//			decrypText = new String( encryptor.decrypt(Hex.decodeHex(encryptText.toCharArray())));
//		} catch (DecoderException e) {
//			// TODO Auto-generated catch block
//			logger.debug(e.getMessage());
//		}
//		logger.debug("encrypted  = " + encryptText );
//		logger.debug("decrypted  = " + decrypText ) ;
//
//
//	}
}
