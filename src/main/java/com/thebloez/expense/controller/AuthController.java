package com.thebloez.expense.controller;

import com.thebloez.expense.config.EncryptDecryptBean;
import com.thebloez.expense.exception.AppException;
import com.thebloez.expense.model.Role;
import com.thebloez.expense.model.RoleName;
import com.thebloez.expense.model.User;
import com.thebloez.expense.payloads.request.LoginRequest;
import com.thebloez.expense.payloads.request.SignUpRequest;
import com.thebloez.expense.payloads.response.ApiResponse;
import com.thebloez.expense.payloads.response.JwtAuthenticationResponse;
import com.thebloez.expense.repository.RoleRepository;
import com.thebloez.expense.repository.UserRepository;
import com.thebloez.expense.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

/**
 * Created by thebloez on 20/06/18.
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    EncryptDecryptBean encryptDecryptBean;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest){

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<Object>(new ApiResponse(false, "Username sudah ada"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<Object>(new ApiResponse(false, "email sudah ada"),
                    HttpStatus.BAD_REQUEST);
        }

        // create user account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());

        //spring encoder
//        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // md5(sha256)
        user.setPassword(encryptDecryptBean.encrypt(user.getPassword()));

        Role userRole = roleRepository.findByName(RoleName.USER)
                .orElseThrow(() -> new AppException("User role belum di set"));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

        URI uriLocation = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        String tesEncrypt = encryptDecryptBean.encrypt("user telah berhasil didaftarkan");

        return ResponseEntity.created(uriLocation).body(
                new ApiResponse(true, encryptDecryptBean.decrypt(tesEncrypt)));
    }


}
