package com.thebloez.expense.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by thebloez on 20/06/18.
 */
@Entity
@Table(name="type")
public class Type extends DateAudit {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idtype;

    private String name;

    private String description;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "type",
            orphanRemoval = true)
    private Set<SubType> subTypes = new HashSet<>();


    public Long getIdtype() {
        return idtype;
    }

    public void setIdtype(Long idtype) {
        this.idtype = idtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<SubType> getSubTypes() {
        return subTypes;
    }

    public void setSubTypes(Set<SubType> subTypes) {
        this.subTypes = subTypes;
    }

}
