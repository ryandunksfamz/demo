package com.thebloez.expense.model;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thebloez on 20/06/18.
 */
@Entity
@Table(name = "product_category")
public class ProductCategory extends UserDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 5, min = 3)
    private String catCode;

    @NotBlank
    @Size(max = 10, min = 3)
    private String catName;

    private Status catStatus;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 30)
    private List<Product> productList = new ArrayList<>();

    public ProductCategory() {
        // default constructor
    }

    public ProductCategory(@NotBlank @Size(max = 5, min = 3) String catCode,
                           @NotBlank @Size(max = 10, min = 3) String catName,
                           Status catStatus,
                           List<Product> productList) {
        this.catCode = catCode;
        this.catName = catName;
        this.catStatus = catStatus;
        this.productList = productList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCatCode() {
        return catCode;
    }

    public void setCatCode(String catCode) {
        this.catCode = catCode;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Status isCatStatus() {
        return catStatus;
    }

    public void setCatStatus(Status catStatus) {
        this.catStatus = catStatus;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void addProduct(Product product){
        productList.add(product);
        product.setCategory(this);
    }

    public void removeProduct(Product product){
        productList.remove(product);
        product.setCategory(this);
    }
}
