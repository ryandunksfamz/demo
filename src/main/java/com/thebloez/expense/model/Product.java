package com.thebloez.expense.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by thebloez on 20/06/18.
 */
@Entity
@Table(name = "product")
public class Product extends UserDateAudit{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String prodName;

    @NotNull
    private BigDecimal priceByIdr;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column
    private Status prodStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cat_id", nullable = false)
    private ProductCategory category;

    public Product() {
        // default constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public BigDecimal getPriceByIdr() {
        return priceByIdr;
    }

    public void setPriceByIdr(BigDecimal priceByIdr) {
        this.priceByIdr = priceByIdr;
    }

    public Status isProdStatus() {
        return prodStatus;
    }

    public void setProdStatus(Status prodStatus) {
        this.prodStatus = prodStatus;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
