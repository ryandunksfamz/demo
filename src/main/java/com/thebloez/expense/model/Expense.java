package com.thebloez.expense.model;

import javax.persistence.*;

/**
 * Created by thebloez on 20/06/18.
 */
@Entity
@Table(name="expense")
public class Expense extends UserDateAudit {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idexpense")
    private Long idexpense;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "amount")
    private Double amount;

    @OneToOne
    @JoinColumn(name = "type_idtype")
    private Type type;

    @OneToOne
    @JoinColumn(name = "subtype_type_idtype")
    private SubType subType;

    @ManyToOne
    @JoinColumn(name = "user_iduser")
    private User user;


    public Long getIdexpense() {
        return idexpense;
    }
    public void setIdexpense(Long idexpense) {
        this.idexpense = idexpense;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public SubType getSubType() {
        return subType;
    }
    public void setSubType(SubType subType) {
        this.subType = subType;
    }

}
