package com.thebloez.expense.model;

/**
 * Created by thebloez on 08/06/18.
 */
public enum RoleName {

    USER, ADMIN;
}
