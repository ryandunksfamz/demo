package com.thebloez.expense.model;

/**
 * Created by thebloez on 20/06/18.
 */
public enum Status {

    AVAILABLE, NOT_AVAILABLE, PENDING;
}
