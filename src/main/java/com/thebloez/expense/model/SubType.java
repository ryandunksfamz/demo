package com.thebloez.expense.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * Created by thebloez on 20/06/18.
 */
@Entity
@Table( name ="sub_type")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="idsubtype")
public class SubType {

    @Id
    @GeneratedValue (strategy=GenerationType.IDENTITY)
    private Long idsubtype;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "idtype", nullable = false)
    private Type type;

    public Long getIdsubtype() {
        return idsubtype;
    }
    public void setIdsubtype(Long idsubtype) {
        this.idsubtype = idsubtype;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }


}
