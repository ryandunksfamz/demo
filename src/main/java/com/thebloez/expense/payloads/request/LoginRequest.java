package com.thebloez.expense.payloads.request;

import javax.validation.constraints.NotBlank;

/**
 * Created by thebloez on 20/06/18.
 */
public class LoginRequest {

    @NotBlank
    private String usernameOrEmail;

    private String password;

    public String getUsernameOrEmail() {
        return usernameOrEmail;
    }

    public void setUsernameOrEmail(String usernameOrEmail) {
        this.usernameOrEmail = usernameOrEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
