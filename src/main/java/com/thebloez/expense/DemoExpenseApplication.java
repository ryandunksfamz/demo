package com.thebloez.expense;

import com.thebloez.expense.config.DbPropBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.util.TimeZone;

@SpringBootApplication
@EntityScan(basePackageClasses = {DemoExpenseApplication.class,
		Jsr310JpaConverters.class
})
public class DemoExpenseApplication {

	@Autowired
	DbPropBean propBean;

	void init(){
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoExpenseApplication.class, args);
	}
}
